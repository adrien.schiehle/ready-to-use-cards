import { GlobalConfiguration, defaultShortcutSettings, defaultUIDataForHand, defaultUIDataForRevealed } from "../constants.js";

/**
 * A configuration sheet to configure shortcuts GUI
 * @extends {FormApplication}
 */
export class ConfigSheetForShortcuts extends FormApplication {


	/** @override */
	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
			id: "rtucards-config-shortcuts",
			classes: ["rtucards-config-shortcuts"],
			template: "modules/ready-to-use-cards/resources/config-shortcuts.hbs",
			width: 600,
			height: "auto",
			closeOnSubmit: false
		});
	}

	/* -------------------------------------------- */

	/** @override */
	get title() {
		return game.i18n.localize("RTUCards.settings.config-shortcuts.menu");
	}

	/* -------------------------------------------- */

	constructor(object={}, options={}) {
		super(object, options);
		this.module = game.modules.get('ready-to-use-cards');
		if(!this.object || this.object == '') {
			this.object = defaultShortcutSettings(game.user);

			this.undo = foundry.utils.duplicate(this.object);
		}
	}

	get currentSettings() {
		const settings = game.settings.get('ready-to-use-cards', GlobalConfiguration.shortcuts);
		if( settings && settings != '') { 
			return settings;
		}
		return defaultShortcutSettings(game.user);

	}


	/** @override */
	async getData() {

		const userImg = (user) => user.isGM ? "modules/ready-to-use-cards/resources/gmIcon.png" : (user.character?.img ?? user.avatar);
		const userCss = (user, value) => {
			let css = value.displayOtherUsers ? "enabled" : "disabled";
			let selection = "unselected";
			if(value.byUsers[user.id] && value.byUsers[user.id].displayed) {
				selection = "selected";
			}
			css += " " + selection;;
			return css;
		};
		const otherUsers = [...game.users.values()].filter(u => u.id != game.user.id);
		otherUsers.sort((u1, u2) => u2.name.localeCompare(u1.name));

		const stacks = Object.entries(this.currentSettings).map( ([key, value]) => {
			const stack = {
				key: key,
				header: {
					label: game.i18n.localize(`RTUCards.settings.config-shortcuts.stack.${key}`),
					used: value.displayed,
					enabled: true,
					css: "header"
				},
				otherUsers: {
					label: game.i18n.localize(`RTUCards.settings.config-shortcuts.stack.${key}ForOthers`),
					used: value.displayOtherUsers,
					enabled: value.displayed,
					css: "otherUsers",
				},
				others: otherUsers.map(u => {
					return {
						id: u.id,
						icon: userImg(u),
						name: u.name,
						css: userCss(u, value),
						stack: key
					};
				}),
				icon: value.icon,
				maxPerLine: value.maxPerLine,
				scalePercent: Math.round(value.scale * 100.0),
			};
			return stack;
		});

		return {
			stacks: stacks
		};
	}

	/** @override */
    activateListeners(html) {
		super.activateListeners(html);

        html.find('.toggle-button.header').click(event => this.#onClickToggleStack(event) );
        html.find('.toggle-button.otherUsers').click(event => this.#onClickToggleDisplayForOthers(event) );
        html.find('.icon-input').change(event => this.#onClickChanceIcon(event) );
        html.find('.max-cards').change(event => this.#onClickUpdateMaxCards(event) );
        html.find('.card-scale').change(event => this.#onClickUpdateScale(event) );
        html.find('.reset-stacks').click(event => this.#onClickRestoreDefault(event) );
        html.find('.other-user.enabled.selected').click(event => this.#onClickToggleOtherDisplay(event, false) );
        html.find('.other-user.enabled.unselected').click(event => this.#onClickToggleOtherDisplay(event, true) );
	}

	/** @override */
	_updateObject(event, formData) {
		// Not used
	}

	async updateSettings(settings) {
		await game.settings.set('ready-to-use-cards', GlobalConfiguration.shortcuts, settings);
		this.module.shortcuts.allHands.forEach(h => h.someSettingsHaveChanged());
		this.module.shortcuts.allRevealed.forEach(r => r.someSettingsHaveChanged());
		this.render();
	}

	/* -------------------------------------------- */

	async #onClickToggleStack(event) {
		event.preventDefault();
		const a = event.currentTarget;
		const stack = a.parentElement.parentElement.dataset.stack;

		const settings = this.currentSettings;
		settings[stack].displayed = !settings[stack].displayed;
		if(!settings[stack].displayed) {
			settings[stack].displayOtherUsers = false;
		}
		await this.updateSettings(settings);
	}

	async #onClickToggleDisplayForOthers(event) {
		event.preventDefault();
		const a = event.currentTarget;
		const stack = a.parentElement.parentElement.dataset.stack;

		const settings = this.currentSettings;
		settings[stack].displayOtherUsers = !settings[stack].displayOtherUsers;
		await this.updateSettings(settings);
	}

	async #onClickToggleOtherDisplay(event, newState) {
		event.preventDefault();
		const img = event.currentTarget;
		const userId = img.dataset.id;
		const stack = img.dataset.stack;

		const settings = this.currentSettings;
		const stackData = settings[stack];
		if(!stackData.byUsers[userId]) {
			stackData.byUsers[userId] = stack=== "hands" ?  defaultUIDataForHand() : defaultUIDataForRevealed();
		}
		stackData.byUsers[userId].displayed = newState;
		await this.updateSettings(settings);
	}

	async #onClickRestoreDefault(event) {
		event.preventDefault();
		await this.updateSettings(defaultShortcutSettings(game.user));
	}

	async #onClickChanceIcon(event) {
		event.preventDefault();
		const a = event.currentTarget;
		const iconPath = a.value;
		const stack = a.parentElement.parentElement.dataset.stack;

		const settings = this.currentSettings;
		settings[stack].icon = iconPath;
		await this.updateSettings(settings);
	}

	async #onClickUpdateMaxCards(event) {
		event.preventDefault();
		const a = event.currentTarget;
		const newValue = parseInt(a.value);
		const stack = a.parentElement.parentElement.dataset.stack;

		const settings = this.currentSettings;
		settings[stack].maxPerLine = newValue;
		await this.updateSettings(settings);
	}

	async #onClickUpdateScale(event) {
		event.preventDefault();
		const a = event.currentTarget;
		const newValue = parseInt(a.value);
		const stack = a.parentElement.parentElement.dataset.stack;

		const settings = this.currentSettings;
		settings[stack].scale = newValue / 100.0;
		await this.updateSettings(settings);
	}


}

